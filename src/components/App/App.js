import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { SocialIcons, Home, Footer, Links } from 'components';
import './App.css';

function App() {
	return (
		<Router>
			<SocialIcons />
			<Switch>
				<Route path='/links' component={Links} />
				<Route path='/' component={Home} exact />
			</Switch>
			<Footer />
		</Router>
	);
};

export default App;