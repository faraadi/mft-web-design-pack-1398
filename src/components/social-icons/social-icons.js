import React from 'react';
import { Card, Ripple, Icon } from 'modules/rmwc';
import MailIcon from './mail.svg';
import TelegramIcon from './telegram.svg';
import './social-icons.css';

export default function SocialIcons() {
    return (
        <Card className='social-icons-container'>
            <Ripple unbounded>
                <a className='social-icon-link' href='mailto:faradivar@gmail.com'>
                    <Icon icon={MailIcon} />
                </a>
            </Ripple>
            <Ripple unbounded>
                <a className='social-icon-link' href="https://t.me/faraadi">
                    <Icon icon={{ icon: TelegramIcon }} />
                </a>
            </Ripple>
        </Card>
    );
};