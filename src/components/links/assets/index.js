export { default as web1Syllabus } from './web1-syllabus.pdf';
export { default as web2Syllabus } from './web2-syllabus.pdf';
export { default as web3Syllabus } from './web3-syllabus.pdf';
export { default as webpackSyllabus } from './webpack-syllabus.pdf';
export { default as web1Checklist } from './mft web1 project checklist.pdf';
export { default as web3Checklist } from './mft web3 project checklist.pdf';
export { default as projectFiles } from './E-Train.zip';