import React from 'react';
import { Card, List, CollapsibleList, SimpleListItem, ListItemMeta } from 'modules/rmwc';
import { Row, Col, Animate } from 'modules';
import { web1Syllabus, web2Syllabus, web3Syllabus, webpackSyllabus, web1Checklist, web3Checklist, projectFiles } from './assets';
import './links.css';

export default function Link() {
    return (
        <Animate>
            <Row gutter={8} justify='center'>
                <Col xs={12} md={4}>
                    <Col xs={12}>
                        <Card className='links-card-container syllabus-container'>
                            <List title='hello world'>
                                <CollapsibleList
                                    handle={<SimpleListItem graphic='assignment_turned_in' text="سرفصل دوره‌ها" metaIcon="chevron_left" />}
                                >
                                    <a className='links-item' href={web1Syllabus} download>
                                        <SimpleListItem text='Web Design I' >
                                            <ListItemMeta icon='cloud_download' />
                                        </SimpleListItem>
                                    </a>
                                    <a className='links-item' href={web2Syllabus} download>
                                        <SimpleListItem text='Web Design II' >
                                            <ListItemMeta icon='cloud_download' />
                                        </SimpleListItem>
                                    </a>
                                    <a className='links-item' href={web3Syllabus} download>
                                        <SimpleListItem text='Web Design III'  >
                                            <ListItemMeta icon='cloud_download' />
                                        </SimpleListItem>
                                    </a>
                                    <a className='links-item' href={webpackSyllabus} download>
                                        <SimpleListItem text='Web Design Pack' >
                                            <ListItemMeta icon='cloud_download' />
                                        </SimpleListItem>
                                    </a>
                                </CollapsibleList>
                            </List>
                        </Card >
                    </Col>
                    <Col xs={12}>
                        <Card className='links-card-container checklist-container'>
                            <List title='hello world'>
                                <CollapsibleList
                                    handle={<SimpleListItem graphic='assignment_turned_in' text="چک‌لیست پروژه پایانی" metaIcon="chevron_left" />}
                                >
                                    <a className='links-item' href={web1Checklist} download>
                                        <SimpleListItem text='Web Design I' >
                                            <ListItemMeta icon='cloud_download' />
                                        </SimpleListItem>
                                    </a>
                                    <a className='links-item' href={web3Checklist} download>
                                        <SimpleListItem text='Web Design III' >
                                            <ListItemMeta icon='cloud_download' />
                                        </SimpleListItem>
                                    </a>
                                </CollapsibleList>
                            </List>
                        </Card >
                    </Col>
                </Col>
                <Col xs={12} md={3}>
                    <Card className='links-card-container'>
                        <List>
                            <a href='https://code.visualstudio.com/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='نرم افزار Visual Studio Code' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://www.google.com/chrome' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='نرم افزار google chrome' metaIcon='exit_to_app' />
                            </a>
                            <a href='https://nodejs.org' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Node.js' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://surge.sh' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Surge' metaIcon="exit_to_app" />
                            </a>
                        </List>
                    </Card>
                    <Card className='project-files-container'>
                        <List>
                            <a href={projectFiles} target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='فایل های پروژه کلاسی وب ۱' metaIcon="exit_to_app" />
                            </a>
                        </List>
                    </Card>
                </Col>
                <Col xs={12} md={3}>
                    <Card className='links-card-container'>
                        <List>
                            <a href='https://developer.mozilla.org/en-US/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Mozilla Developer Network' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://dev.to/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Dev.To' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://hackernoon.com/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Hackernoon' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://www.freecodecamp.org/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Free Code Camp' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://css-tricks.com/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='CSS Tricks' metaIcon="exit_to_app" />
                            </a>
                            <a href='https://scotch.io/' target='_blank' rel='noopener noreferrer'>
                                <SimpleListItem text='Scotch' metaIcon="exit_to_app" />
                            </a>
                        </List>
                    </Card >
                </Col>
            </Row>
        </Animate>
    );
};