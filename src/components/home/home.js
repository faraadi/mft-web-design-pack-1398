﻿import React from 'react';
import { Card, Button } from 'modules/rmwc';
import { Row, Col, Animate } from 'modules';
import { exercies } from 'consts';
import './home.css';

export default function Home() {
    return (
        <Animate>
        <Card className='exersices-card-container'>
            <Row className='exersices-container' gutter={8} justify='center'>
                {
                    exercies.map((item, key) => (
                        <Col xs={12} md={4} key={key} className='text-center'>
                            <Button raised >
                                <a href={item.url} target="_blank" rel='noopener noreferrer'>{item.name}</a>
                            </Button>
                        </Col>
                    ))
                }
            </Row>
        </Card>
        </Animate>
    );
};