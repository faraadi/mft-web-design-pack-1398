export { default as App } from './App/App';
export { default as Home } from './home/home';
export { default as Footer } from './footer/footer';
export { default as Links } from './links/links';
export { default as SocialIcons } from './social-icons/social-icons';