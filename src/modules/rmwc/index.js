import '@material/card/dist/mdc.card.css'
import '@material/button/dist/mdc.button.css';
import '@rmwc/icon/icon.css';
import '@material/ripple/dist/mdc.ripple.css';
import '@material/list/dist/mdc.list.css';
import '@rmwc/list/collapsible-list.css';
import './rmwc.css';
import { List } from '@rmwc/list';

export { Card } from '@rmwc/card';
export { Button } from '@rmwc/button';
export { Icon } from '@rmwc/icon';
export { Ripple } from '@rmwc/ripple';
export { List, CollapsibleList, SimpleListItem, ListItemMeta } from '@rmwc/list';

// function test() {
//     return <List ></List>
// }