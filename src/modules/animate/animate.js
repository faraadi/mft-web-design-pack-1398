import React from 'react';
import { VelocityTransitionGroup } from 'velocity-react';
import 'velocity-animate/velocity.ui';
import './animate.css';

const enter = {
    fade: {
        animation: "transition.fadeIn",
        stagger: 50,
        duration: 200,
        display: null,
        visibility: 'visible',
        delay: 0
    },
    slide: {
        animation: "transition.slideUpBigIn",
        duration: 200,
        visibility: 'visible',
        display: null,
        delay: 0
    }
};

const leave = {
    fade: {
        stagger: 50,
        duration: 200,
        display: null,
        visibility: 'visible',
        delay: 0
    },
    slide: {
        animation: "transition.slideUpBigOut",
        duration: 200,
        visibility: 'visible',
        display: null,
        delay: 0
    }
};

const easing = [0.4, 0.0, 0.2, 1];

export default function AgAnimate(props) {
    return (
        <VelocityTransitionGroup
            {...props}
            children={props.children}
            enter={enter[props.type]}
            leave={leave[props.type]}
            easing={easing}
            runOnMount
            enterHideStyle={{ visibility: 'visible' }}
            enterShowStyle={{ visibility: 'hidden' }}
            className="animate-container"
        />
    );
};

AgAnimate.defaultProps = {
    type: "slide"
};