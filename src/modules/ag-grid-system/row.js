import React from 'react';

const rowClassName = "ag-row";
const gutterPrefix = " ag-row-gutter-";
const justifyPrefix = " ag-row-justify-";
const alignPrefix = " ag-row-align-";
const directionPrefix = " ag-row-direction-";

export default function AgRow(props) {
    const gutterClassName = props.gutter ? gutterPrefix + props.gutter : "";
    const justifyClassName = props.justify ? justifyPrefix + props.justify : "";
    const alignClassName = props.align ? alignPrefix + props.align : "";
    const directionClassName = props.direction ? directionPrefix + props.direction : "";
    return (
        <div onClick={props.onClick} className={rowClassName + gutterClassName + justifyClassName + alignClassName + directionClassName +  " " + props.className}>
            {props.children}
        </div>
    );
};

AgRow.defaultProps = {
    className: ""
};

/*
    gutter sizes: [8, 16, 24, 36]
    justify: ["start", "end", "center", "space-between", "space-around", "space-evenly"]
    align: ["start", "end", "center"]
    direction: ["column", "row"]
*/