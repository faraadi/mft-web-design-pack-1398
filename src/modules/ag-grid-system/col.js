import React from 'react';

const xsPrefix = " ag-col-xs-";
const smPrefix = " ag-col-sm-";
const mdPrefix = " ag-col-md-";
const lgPrefix = " ag-col-lg-";
const xlPrefix = " ag-col-xl-";
const colClassName = "ag-col";

export default function AgCol(props) {
    const xsClassName = props.xs ? xsPrefix + props.xs : "";
    const smClassName = props.sm ? smPrefix + props.sm : "";
    const mdClassName = props.md ? mdPrefix + props.md : "";
    const lgClassName = props.lg ? lgPrefix + props.lg : "";
    const xlClassName = props.xl ? xlPrefix + props.xl : "";
    return (
        <div onClick={props.onClick} className={colClassName + xsClassName + smClassName + mdClassName + lgClassName + xlClassName + " " + props.className}>
            {props.children}
        </div>
    );
};

AgCol.defaultProps = {
    className: ""
};

/*
    xs: [0,1,2,3,4,5,6,7,8,9,10,11,12,"auto"]
    sm: [0,1,2,3,4,5,6,7,8,9,10,11,12,"auto"]
    md: [0,1,2,3,4,5,6,7,8,9,10,11,12,"auto"]
    lg: [0,1,2,3,4,5,6,7,8,9,10,11,12,"auto"]
    xl: [0,1,2,3,4,5,6,7,8,9,10,11,12,"auto"]
*/